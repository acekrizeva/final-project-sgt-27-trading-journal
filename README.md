## Trading Journal
This is the Final Project of She Goes Tech IT training program.

## Description
Trading Journal is a handy approach to storing and analyzing trades for any active financial trader. Profound analysis of the past trades helps to avoid repeating mistakes, thus generating a bigger profit in the future.

MySQL database enables to store exact trades by entering the following data: Trade ID, Trader's full name, Execution date, Instrument type, Ticker name, Trade size, Trade type, Entry price, Exit price, Trade result, Duration, Strategy applied.

Java methods, in turn, enable to analyze data according to the following criteria: Check all the trades, Insert a new trade, Remove a trade, Calculate average profit/ loss of all trades, Check minimal profit/ loss, Check maximal profit/ loss, Calculate certain trader's profit, Calculate Capital Gains Tax.

## Visuals

DB Structure 

![DB_Structure](.images/DB_Structure.jpg)

Flowchart 

![Flowchart](.images/Flowchart.JPG)

## Installation
In order to run the Trading Journal the following software needs to be installed first:
* Java Development Kit (JDK)
* IntelliJ IDEA
* MySQL

## Authors and acknowledgment
The Project has been developed thanks to @acekrizeva, @evelinaalimova, @OHludneva and @savicka.anastasija.

## Project status
The Project is considered to be complete without any future maintenance options.
