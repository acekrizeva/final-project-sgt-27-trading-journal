package TradingJournal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {
    private static user User;

    static {
        User = new user();
    }

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Trading Journal");
        System.out.println("Press\n" +
                "0 - To log in\n" +
                "1 - To register\n" +
                "Any key to quit\n");
        String choice = scanner.nextLine();
        switch (choice) {
            case "0":
                User = LogIn();
                break;
            case "1":
                User = Register();
                break;
            default:
                return;
        }
        initialize();
    }

    public static void initialize() {

        final String dbURL = "jdbc:mysql://localhost:3306/java27";
        final String user = "root";
        final String password = "Sgtjava2701!";

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            boolean quit = false;
            int choice;

            database.printInstructions();

            while (!quit) {
                System.out.println("Enter your choice: ");
                choice = scanner.nextInt();
                scanner.nextLine();

                switch (choice) {
                    case 0:
                        //print all options
                        database.printInstructions();
                        break;
                    case 1:
                        //To print all lines from the Trading Journal
                        database.readData(conn);
                        break;
                    case 2:
                        //To insert a new line
                        database.insertData(conn);
                        break;
                    case 3:
                        //To remove a line from the Trading Journal
                        System.out.println("Enter Trade ID you want to delete: ");
                        int tradeID = scanner.nextInt();

                        database.deleteData(conn, tradeID);
                        break;
                    case 4:
                        //To calculate the average profit or loss
                        database.averageProfitLoss(conn);
                        break;
                    case 5:
                        //To see min profit/loss
                        database.minProfitLoss(conn);
                        break;
                    case 6:
                        //To see max profit/loss
                        database.maxProfitLoss(conn);
                        break;
                    case 7:
                        //To calculate total profit or loss
                        database.totalProfitLoss(conn);
                        break;
                    case 8:
                        //One trader's profit
                        System.out.println("Enter Last name: ");
                        String profitLastName = scanner.nextLine();

                        database.oneTraderProfit(conn, profitLastName);
                        break;
                    case 9:
                        //To calculate Capital Gains Tax
                        System.out.println("Enter Last name: ");
                        String taxLastName = scanner.nextLine();

                        database.oneTraderTax(conn, taxLastName);
                        break;
                    case 10:
                        quit = true;
                        break;
                }
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong");
        }
    }

    private static user LogIn() {
        return database.logIn();
    }

    private static user Register() {
        return database.userRegistration();
    }
}
class user {
    public String username;
    public String firstName;
    public String lastName;
    public String password;

    public void setUserName (String userName) {
        this.username = userName;
    }
}
