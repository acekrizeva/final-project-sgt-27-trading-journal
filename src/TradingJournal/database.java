package TradingJournal;

import java.sql.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class database {
    static final String dbURL = "jdbc:mysql://localhost:3306/java27";
    static final String user = "root";
    static final String password = "Sgtjava2701!";

    static final user existingUser = new user();

    static Scanner scanner = new Scanner(System.in);

    public static user logIn() {
        String userName;
        String pswrd;

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            do {
                System.out.println("Please enter your username");
                userName = scanner.nextLine().trim();
                existingUser.setUserName(userName);
                System.out.println("Please enter your password");
                pswrd = scanner.nextLine().trim();
            } while (checkUserNameAndPass(conn, userName, pswrd)!= 1);
            //userMenu();
        } catch (SQLException e) {
            System.out.println("No connection " + e);
        }
        return null;
    }

    public static int checkUserNameAndPass (Connection conn, String username, String password) throws SQLException {

        String sql = "SELECT count(*) FROM users WHERE username = ? AND password = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);
        ResultSet resultSet = preparedStatement.executeQuery();
        int count = 0;
        if (resultSet.next()) {
            count = resultSet.getInt(1);
            if (count == 1) {
                System.out.println("Connected");

            } else {
                System.out.println("Username or password is incorrect, please try again");
            }
        }
        return count;
    }
    public static user userRegistration(){
        System.out.println("Registration...");
        Scanner scanner = new Scanner(System.in);
        try(Connection conn = DriverManager.getConnection(dbURL, user , password)){

            boolean matches = false;
            while (!matches){
                System.out.println("Please enter username");
                String username = scanner.nextLine();

                Pattern patternID = Pattern.compile("[a-zA-Z0-9]*");
                Matcher matcher = patternID.matcher(username);
                boolean validFormat = matcher.matches();
                if(validFormat){

                    String sql = "SELECT * FROM users WHERE username = ?";
                    PreparedStatement preparedStatement = conn.prepareStatement(sql);
                    preparedStatement.setString(1, username);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    // No rows returned
                    if(!resultSet.next()) {
                        System.out.println("Proceed with next step");
                        existingUser.username = username;
                        matches = true;
                    }
                }else{
                    System.out.println("Please try again, trader with this ID already exists");
                }
            }

            matches = false;
            while (!matches){
                System.out.println("Please enter your password");
                System.out.println("6 letters and 3 numbers");
                String pswrd = scanner.nextLine();
                Pattern patternPswrd = Pattern.compile("[a-zA-Z]{6}[0-9]{3}");
                Matcher matcher = patternPswrd.matcher(pswrd);
                matches = matcher.matches();
                if(matches){
                    System.out.println("Please proceed with next step");
                    existingUser.password = pswrd;
                }else{
                    System.out.println("Please try another password!");
                }
            }
            matches = false;
            while (!matches){
                System.out.println("Enter your name");
                String firstName = scanner.nextLine().toUpperCase().trim();
                Pattern patternName = Pattern.compile("[a-zA-Z]*");
                Matcher matcher = patternName.matcher(firstName);
                matches = matcher.matches();
                if (matches){
                    System.out.println("Name entered, please continue");
                    existingUser.firstName = firstName;
                }else {
                    System.out.println("Please try once again");
                }
            }

            matches = false;
            while (!matches){
                System.out.println("Please enter your last name");
                String lastName = scanner.nextLine().toUpperCase().trim();
                Pattern patternLast = Pattern.compile("[a-zA-z]*");
                Matcher matcher = patternLast.matcher(lastName);
                matches = matcher.matches();
                if(matches){
                    System.out.println("Last name entered");
                    existingUser.lastName = lastName;
                }else{
                    System.out.println("Please try another last name");
                }
            }
            insertUser();
        } catch (SQLException e) {
            System.out.println("No connection " + e);
        }
        return existingUser;
    }

    private static void insertUser() throws SQLException{
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "INSERT INTO users (username, password, name, lastname) Values (?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, existingUser.username);
            preparedStatement.setString(2, existingUser.password);
            preparedStatement.setString(3, existingUser.firstName);
            preparedStatement.setString(4, existingUser.lastName);
            int rowInserted = preparedStatement.executeUpdate();
            if (rowInserted > 0) {
                System.out.println("Your account was created successfully");

            } else {
                System.out.println("Something went wrong");
            }
        }
    }

    public static void printInstructions() {
        System.out.println("Press\n" +
                "0 - To print instructions\n" +
                "1 - To print all lines from the Trading Journal\n" +
                "2 - To insert a new line\n" +
                "3 - To remove a line from the Trading Journal\n" +
                "4 - To calculate the average profit or loss of all traders\n" +
                "5 - To see min profit/loss \n" +
                "6 - To see max profit/loss \n" +
                "7 - To calculate total profit or loss \n" +
                "8 - To calculate one trader's profit \n" +
                "9 - To calculate Capital Gains Tax \n" +
                "10 - To quit the application");
    }

    public static void readData(Connection conn) throws SQLException {
        String sql = "SELECT * FROM trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            int tradeID = resultSet.getInt(1);
            String firstName = resultSet.getString(2);
            String lastName = resultSet.getString(3);
            String date = resultSet.getString(4);
            String instrument = resultSet.getString(5);
            String ticker = resultSet.getString(6);
            int lots = resultSet.getInt(7);
            String longShort = resultSet.getString(8);
            double entryPrice = resultSet.getDouble(9);
            double exitPrice = resultSet.getDouble(10);
            double profitLoss = resultSet.getDouble(11);
            String duration = resultSet.getString(12);
            String strategy = resultSet.getString(13);

            String output = "Trade info: \n\t ID: %d \n\t First name: %s \n\t " +
                    "Last name: %s \n\t Date: %s \n\t Instrument: %s \n\t " +
                    "Ticker: %s \n\t Lots: %s \n\t Long or Short: %s \n\t " +
                    "Entry price: %f \n\t Exit price: %f \n\t Profit or Loss: %f \n\t " +
                    "Duration: %s \n\t Strategy: %s";

            System.out.printf((output) + "%n", tradeID, firstName, lastName, date, instrument, ticker, lots, longShort, entryPrice, exitPrice, profitLoss, duration, strategy);
        }
    }

    public static void insertData(Connection conn) throws SQLException {

        String sql = "INSERT INTO Trading_journal (firstName,lastName,date,instrument,ticker,lots,longShort,entryPrice,exitPrice,profitLoss,duration,strategy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";

        System.out.println("Enter First name: ");
        String firstName = scanner.nextLine();

        System.out.println("Enter Last name: ");
        String lastName = scanner.nextLine();

        System.out.println("Enter date (YYYY-MM-DD): ");
        String date = scanner.nextLine();

        System.out.println("Enter instrument: ");
        String instrument = scanner.nextLine();

        System.out.println("Enter ticker: ");
        String ticker = scanner.nextLine();

        System.out.println("Enter lots: ");
        String lots = scanner.nextLine();

        System.out.println("Long or Short: ");
        String longShort = scanner.nextLine();

        System.out.println("Enter entry price: ");
        double entryPrice = scanner.nextDouble();

        System.out.println("Enter exit price: ");
        double exitPrice = scanner.nextDouble();

        System.out.println("Profit or Loss: ");
        double profitLoss = scanner.nextDouble();
        scanner.nextLine();

        System.out.println("Enter duration: ");
        String duration = scanner.nextLine();

        System.out.println("Enter strategy: ");
        String strategy = scanner.nextLine();

        PreparedStatement preparedStatement = conn.prepareStatement(sql);

        preparedStatement.setString(1, firstName);
        preparedStatement.setString(2, lastName);
        preparedStatement.setString(3, date);
        preparedStatement.setString(4, instrument);
        preparedStatement.setString(5, ticker);
        preparedStatement.setString(6, lots);
        preparedStatement.setString(7, longShort);
        preparedStatement.setDouble(8, entryPrice);
        preparedStatement.setDouble(9, exitPrice);
        preparedStatement.setDouble(10, profitLoss);
        preparedStatement.setString(11, duration);
        preparedStatement.setString(12, strategy);

        int rowInserted = preparedStatement.executeUpdate();

        if (rowInserted > 0){
            System.out.println("A new trade was inserted successfully");
        }else{
            System.out.println("Something went wrong");
        }
    }

    public static void deleteData(Connection conn, int TradeID) throws SQLException {
        String sql = "DELETE FROM Trading_journal WHERE TradeID = ?";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, TradeID);

        int rowDeleted = preparedStatement.executeUpdate();

        if (rowDeleted > 0) {
            System.out.println("A trade was deleted successfully");
        } else {
            System.out.println("Something went wrong");
        }
    }

    public static void averageProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT ROUND(AVG(ProfitLoss),2) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double average = resultSet.getDouble(1);
            System.out.println("The average profit or loss of all traders is: " + average);
        }
    }

    public static void minProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT MIN(ProfitLoss) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double min = resultSet.getDouble(1);
            System.out.println("The min profit or loss of all traders is: " + min);
        }
    }

    public static void maxProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT MAX(ProfitLoss) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double min = resultSet.getDouble(1);
            System.out.println("The max profit or loss of all traders is: " + min);
        }
    }

    public static void totalProfitLoss(Connection conn) throws SQLException {
        String sql = "SELECT SUM(ProfitLoss) FROM Trading_journal;";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            double total = resultSet.getDouble(1);
            System.out.println("Total profit or loss of all traders is: " + total);
        }
    }

    public static void oneTraderProfit(Connection conn, String lastName) throws SQLException {
        String sql = "SELECT SUM(ProfitLoss) FROM Trading_journal WHERE  LastName = ?";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, lastName);

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            double profit = resultSet.getDouble(1);
            System.out.println("Total profit of trader " + lastName + " is " + profit);
        }
    }

    public static void oneTraderTax(Connection conn, String lastName) throws SQLException {
        String sql = "SELECT SUM(ProfitLoss) FROM Trading_journal WHERE LastName = ?";

        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, lastName);

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            double profit = resultSet.getDouble(1);
            if (profit >= 0) {
                double tax = profit * 0.2;
                System.out.printf("Your Capital Gains Tax due is %.2f \n", tax);
            } else {
                System.out.println("You don't have to pay any tax due to capital loss");
            }
        }
    }
}
